<?php
	session_start();
	header('content-type: text/html; charset=utf-8');
	require 'phpsqlinfo_dbinfo.php';
	require 'headtag.php';
?>

<!DOCTYPE html>
<html>
	<?php createHead(array("title" => $nomePagina . $nome_site)); ?>

	<body class="corposite">
		<?php require 'header.php'; ?>
		<div class="div_centro">
			<div class="font8" style="min-height: 400px;">
				<h3>Objetivo:</h3>
				<span style="padding-left:20px;">
					 Seu principal objetivo é auxiliar no desenvolvimento de sistemas web que utilizam informação geográfica voluntária (VGI).
					O ClickOnMap, possui uma interface amigável e permite alterar informações para criação rápida e intuitiva, de um Geobrowser,
					que é um web site que coleta e disponibiliza informação geográfica voluntária. Como exemplo de sistemas que utilizam VGI,
					podemos destacar o <a href="http://www.wikicrimes.org/" target="_blanck">WikiCrimes</a> e o
					<a href="http://www.teofilootoni.mg.gov.br/" target="_blanck">Cidadão Teófilo Otoni-MG</a>.
				</span>
			</div>
			<br/>
		</div>

        <?php include 'partials/rodape.php'; ?>
   </body>
</html>
