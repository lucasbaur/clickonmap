<?php
    session_start();
    header('content-type: text/html; charset=utf-8');
    require 'phpsqlinfo_dbinfo.php';
    require 'headtag.php';
?>

<!DOCTYPE html>
<html>
    <?php createHead(array("title" => $nomePagina . $nome_site, "script" => array("src/jquery.min.js", "scripts/positioning.js"), "required" => array("filtro_gramatical.php"))); ?>

    <body onload="init()">
        <?php require 'header.php'; ?>
        <div style="width: 50%; display: block;">
                <?php echo filtro("Até uma perícia já foi feita", true); ?>
        </div>
   </body>
</html>
