<?php
require 'phpsqlinfo_dbinfo.php';
$query = "SELECT desTituloAssunto, desColaboracao, datahoraCriacao,codColaboracao
		  FROM colaboracao
		  WHERE tipoStatus != 'R'
		  ORDER BY datahoraCriacao DESC
		  LIMIT 5";
$result = $connection->query($query);

//Se tem colaboracoes
if ($result->num_rows > 0){

    echo("<label class='text'><b>Últimas colaborações</b></label><br><br>
            <table class='hor-minimalist-b'>
                <thead>
                    <tr>
                        <th scope='col'>Título</th>
                        <th scope='col'>Descrição</th>
                        <th scope='col' style='text-align:center'>Data</th>
                    </tr>
                </thead><tbody>");

    $temp_0 = array();
    while ($row = $result->fetch_array(MYSQLI_NUM))
    {
        $html_temp = "<tr onclick='enviar(" . $row[3] . ")'><td>" . substr($row[0], 0, 16);
        $temp_0 = $row[0];
        if (strlen($temp_0) >= 15) $html_temp .= "...";
        else $html_temp .= "";

        $html_temp .= "</td><td>".substr($row[1], 0, 26);
        $temp_1 = $row[1];
        if (strlen($temp_1) >= 25) $html_temp .= "...";
        else $html_temp .= "";

        $html_temp .= "</td><td style='text-align:center'>" . date('d/m/Y', strtotime($row[2])) . "</td></tr>";
        echo ($html_temp);
    }
    echo("</tbody></table>");
}