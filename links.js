'use strict';

function sobre() { window.location.href = 'sobre.php'; }
function index() { window.location.href = 'index.php'; }
function tutorial() { window.location.href = 'tutorial.php'; }
function registro() { window.location.href = 'registro.php'; }
function colaborar() { window.location.href = 'colaborar.php'; }
function filtros() { window.location.href = 'filtros.php'; }
function analise_dados() { window.location.href = 'analise_dados.php'; }
function ranking_usuario() { window.location.href = 'ranking_usuario.php'; }
function dados_historicos() { window.location.href = 'dados_historicos.php'; }
function estatisticas() { window.location.href = 'estatisticas.php'; }
function contato() { window.location.href = 'contato.php'; }