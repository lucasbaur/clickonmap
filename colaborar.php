<?php
	session_start();
	header('content-type: text/html; charset=utf-8');
	require 'headtag.php';
	require 'phpsqlinfo_dbinfo.php';

	if(!isset($_SESSION['user_'.$link_inicial]) && !isset($_SESSION['pass_'.$link_inicial]))
		header("location: registro.php");
	else
	{
?>

<!DOCTYPE html>
<html>
	<?php
	createHead(
		array("title" => $nomePagina . $nome_site,
		"script" => array(
                    // "http://maps.google.com/maps/api/js?libraries=places,visualization",
                    "https://maps.googleapis.com/maps/api/js?key=AIzaSyAhE0kcLiR1qn0pcUJ5UVCo7xSyBL3VE6Y&libraries=places,visualization,geometry",
					"src/jquery.form.js",
					"jsor-jcarousel/lib/jquery.jcarousel.min.js",
					"src/jquery.blockUI.js",
					"links.js",
					"src/util.js",
					"tabelas_dinamicas.js",
					"map.js",
                    "bootstrap/js/filestyle.min.js",
                    "bootstrap/datetimepicker/js/bootstrap-datetimepicker.js",
                    "bootstrap/datetimepicker/js/locales/bootstrap-datetimepicker.pt-BR.js",
                    "src/moment-js/moment.js",
                    "src/markerclusterer_packed.js"),
		"css" => array(
                "http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css",
                "bootstrap/datetimepicker/css/bootstrap-datetimepicker.min.css",
                "config.css",
				"jsor-jcarousel/skins/tango/skin.css"),
		"required" => array("calendar/tc_calendar.php",
					   "colaborar.js.php")));
	?>

	<body id='wholething' onload="initialize()">

		<?php require 'header.php'; ?>

		<div class="div_centro">
            <div class="row">
                <div class="col-md-2">
                    <a onclick="abreTutorial();"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-book"></span> Tutorial</button></a>
                </div>
                <div class="col-md-10">
                    <?php include 'partials/pesquisar_endereco.php'; ?>
                </div>
            </div>
		</div>

		<div id="map_canvas" class="map_canvas"></div>
		<br />

		<div class="div_centro">
			<div class="centro">
                <?php include 'partials/heatmap_opcoes.php'; ?>
			</div>
            <?php include 'partials/tabela_estatistica_rodape.php'; ?>
		</div>

        <!-- Modal -->
        <div id="modal-keywords" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" onclick="atualizaLongLat();">&times;</button>
                        <h3 class="modal-title">Envio de palavras-chave</h3>
                    </div>
                    <div class="modal-body">
                        <p class="text-left">Informe até 10 palavras-chave separadas por ponto e vírgula (;):</p>
                        <div class="form-group">
                            <label for="comment">Palavras-chave:</label>
                            <textarea class="form-control" rows="5" id="keywordstxt" maxlength="150"></textarea>
                        </div>
                        <span class="small">Ex: perigo; infestação...</span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="atualizaLongLat();">Pular</button>
                        <button type="button" class="btn btn-success" onclick="save_keywords()">Enviar</button>
                    </div>
                </div>

            </div>
        </div>

        <!-- Modal -->
        <div id="info-selos" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Selos de conquista: Número de colaborações 1</h4>
                    </div>
                    <div class="modal-body text-center">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="images/selos/numeroDeColaboracoes1.png" class="img-responsive" style="margin: 0 auto;">
                            </div>

                            <div class="col-md-9">
                                <div class="row text-center" style="margin: 0;">
                                    <p>Você ganhou um selo de conquista por ter realizado sua primeira colaboração. Você pode ver seus selos em seu perfil.</p>
                                </div>
                                <div class="row text-center"  style="margin: 20px 0 0 0;">
                                    <a href='user_profile.php?uid=<?php echo $_SESSION['code_user_'.$link_inicial]; ?>' title='Ver perfil' target="_blank">
                                        <button type="button" class="btn btn-warning active"><span class="glyphicon glyphicon-user"></span> <strong> Ver Perfil</strong></button>
                                    </a>
                                    <a href="selos.php" target="_blank"><button type="button" class="btn btn-warning active"><span class="glyphicon glyphicon-eye-open"></span><strong> Ver Selos</strong></button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </div>
                </div>

            </div>
        </div>

        <script>
            $(function()
            {
                var userid = <?php echo $_SESSION['code_user_' . $link_inicial]; ?>;
                var ifmsg;

                $.ajax(
                {
                    method: 'POST',
                    url: 'sem_dicas.php',
                    data: {func: 'seen', user: userid},
                    async: false
                }).done(function(data) { ifmsg = data; });

                if (ifmsg != 0) viewhelp = false;
            });

            $('#map_canvas').on('mode_changed', function()
            {
                $('#helpopover').popover(
                {
                    content: function()
                    {
                        if (mapmode == 'colab') return "Clique em qualquer ponto do mapa para iniciar uma colaboração. Utilize a ferramenta de zoom (Parte superior direita do mapa) para aumentar a precisão de sua colaboração.";
                        else if (mapmode == 'vis') return "Seja bem vindo(a)! Para realizar uma colaboração, altere o modo clicando na opção \"Colaborar\" na parte superior esquerda do mapa.";
                        else if (mapmode == 'winop') return "Preencha os dados do formulário e clique no botão \"Enviar Colaboração\".";
                        else if (mapmode == 'keys') return "Caso queira, informe algumas palavras-chave que descrevam sua colaboração e clique em \"Enviar\", caso contrário clique no botão \"Pular\"";
                    },
                    placement: 'right'
                });

                if (viewhelp)
                {
                    $('#helpopover').popover('show');
                    $('#helpopover').data("bs.popover").inState.click = true;
                }
            });

            $('body').on('shown.bs.popover', function() { $('#helpopovertext').html('Fechar'); });
            $('body').on('hidden.bs.popover', function() { $('#helpopovertext').html('Ajuda '); });

            $(document).on('click', '#helpopover', function() { viewhelp = !viewhelp; });

            $(document).one('click', '#helpopover', function()
            {
                var userid = <?php echo $_SESSION['code_user_' . $link_inicial]; ?>;
                $.ajax(
                {
                    method: 'POST',
                    url: 'sem_dicas.php',
                    data: {func: 'ignore', user: userid}
                });

                viewhelp = false;
            });

            $('#modal-keywords').on('hidden.bs.modal', function(event)
            {
                // Close the messsage here
                mapmode = 'colab';
                $('#helpopover').popover('hide');
                $('helpopovertext').html('Ajuda');
            });
        </script>

        <?php include 'partials/rodape.php'; ?>
   </body>
</html>
<?php
	}
