<?php
    require '../phpsqlinfo_dbinfo.php';
    session_start();
    if(!isset($_SESSION['user_admin_'.$link_inicial]) && !isset($_SESSION['pass_admin_'.$link_inicial]))
        header("location: naologado_admin.html");
    else
    {
?>

    <html>
        <?php
        $id = '';

        if(isset($_GET['id'])) $id = $_GET['id'];

        // Consultas SQL
        $query = $connection->query("SELECT * FROM selosconquistas WHERE idSelo = '$id'");
        if(!($row = $query->fetch_assoc()))
        {
            echo "Erro na consulta: Não foram encontrados dados na tabela selosconquistas";
            exit;
        }

        $nomeSelo = $row['nomeSelo'];
        $descSelo = $row['descricaoSelo'];

        require 'cabecalho.php';
        require 'menu.php';
        ?>

        <div id="toolbar-box">
            <div class="m">
                <div class="toolbar-list" id="toolbar">
                    <ul>
                        <li class="button" id="toolbar-apply">
                        <a href="#" onclick="salvar(<?php echo $id; ?>)" class="toolbar">
                            <span class="icon-32-apply"></span>
                            Salvar
                        </a>
                        </li>

                        <li class="divider"></li>

                        <li class="button" id="toolbar-cancel">
                            <a href="listar_selos.php" class="toolbar">
                                <span class="icon-32-cancel"></span>
                                Cancelar
                            </a>
                        </li>
                    </ul>
                    <div class="clr"></div>
                </div>
                <div class="pagetitle icon-48-category-edit"><h2>Editar selo</h2></div>
            </div>
        </div>

        <div id="element-box">
            <div class="m">
                <form action="edita_selo.php" id="application-form" method="post" name="adminForm" class="form-validate">
                    <div
                        <div class="tab" style="display: block;">
                            <div class="noshow">
                                <div class="width-60 fltlft">
                                    <div class="width-100">
                                        <fieldset class="adminform">
                                            <legend>Editar selo</legend>
                                            <ul class="adminformlist">
                                                <input type="hidden" id="idSelo" name="idSelo" value="<?php echo $id; ?>" />
                                                <li>
                                                    <label for="jform_novonomeselo" class="hasTip required" title="" aria-invalid="false">
                                                        Nome
                                                        <span class="star">&nbsp;*</span>
                                                    </label>
                                                    <input type="text" id="jform_novonomeselo" name="nnome" value="<?php echo $nomeSelo; ?>" size="50" aria-invalid="false" />
                                                </li>

                                                <li>
                                                    <label for="jform_descricao" class="hasTip required" title="" aria-invalid="false">
                                                        Descrição
                                                        <span class="star">&nbsp;*</span>
                                                    </label>
                                                    <!-- If you put the php part in a new line, it messes up the text area by adding a bunch of blank spaces / tabs -->
                                                    <textarea id="jform_descricao" name="ndesc" aria-invalid="false" rows="10" cols="50"><?php echo $descSelo; ?></textarea>
                                                </li>
                                            </ul>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="clr"></div>
            </div>
        </div>
    </html>
    <script type="text/javascript" src="../src/jquery-ui-1.11.4.custom/external/jquery/jquery.js"></script>
    <script  type="text/javascript">
        function salvar(id)
        {
            if($('#jform_novonomeselo').val() == '') alert("Informe um novo nome para o selo!");
            else
            {
                $('#application-form').submit();
            }
        }
    </script>
<?php
        require 'rodape.php';
    }
?>
