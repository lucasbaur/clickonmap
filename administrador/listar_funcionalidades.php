<?php
    require '../phpsqlinfo_dbinfo.php';
    session_start();
    if(!isset($_SESSION['user_admin_' . $link_inicial]) && !isset($_SESSION['pass_admin_' . $link_inicial]))
        header("location: naologado_admin.html");
    else
    {
?>

    <html>
        <?php
            //Consultas SQL
            $consulta = $connection->query("SELECT codFuncionalidade, nomeFuncionalidade, descFuncionalidade, statusFuncionalidade
                                            FROM funcionalidadestratamentotexto");
            $numFuncs = $consulta->num_rows;

            require 'cabecalho.php';
            require 'menu.php';
        ?>

        <div id="toolbar-box">
            <div class="m">
                <div class="toolbar-list" id="toolbar">
                        <li class="button" id="toolbar-new">
                            <a href="#" onclick="habilitar(<?php echo $numFuncs; ?>, 1)" class="toolbar">
                                <span class="icon-32-publish"></span>
                                Habilitar
                            </a>
                        </li>

                        <li class="button" id="toolbar-new">
                            <a href="#" onclick="habilitar(<?php echo $numFuncs; ?>, 0)" class="toolbar">
                                <span class="icon-32-unpublish"></span>
                                Desabilitar
                            </a>
                        </li>
                    </ul>
                    <div class="clr"></div>
                </div>
                <div class="pagetitle icon-48-info"><h2>Tratamento de texto</h2></div>
            </div>
        </div>

        <div id="element-box">
            <div class="m">
                <form action="#" id="application-form" method="post" name="adminForm" class="form-validate">
                    <table class="adminlist">
                        <thead>
                            <tr>
                                <th width="1%">
                                    <input type="checkbox" name="selecionaTodas" id="selecionaTodas" value="" title="Selecionar todas as palavras" onclick="selecionar(<?php echo $numFuncs; ?>)">
                                </th>
                                <th>Descrição da funcionalidade</th>
                                <th width="20%"">Status</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            $i = 0;
                            while($func = $consulta->fetch_array())
                            {
                                echo '<tr class="row' . ($i % 2) . '">';
                                    echo '<td class="center">';
                                        echo '<input type="checkbox" id="cb' . $i . '" name="cid[]" value="' . $func['codFuncionalidade'] . '" title="Selecionar esta funcionalidade">';
                                    echo '</td>';

                                    if ($func['nomeFuncionalidade'] == "correcao")
                                    {
                                        // Link para adicionar palavras ao dicionario
                                        echo '<td><a style="font-size: 0.85em;" href="listar_dicionario.php" title="Adicionar palavras ao dicionário">';
                                        echo $func['descFuncionalidade'];
                                        echo '</a></td>';
                                    }
                                    else echo '<td><span style="font-size: 0.85em;">' . $func['descFuncionalidade'] . '</td>';

                                    echo '<td>';
                                        if ($func['statusFuncionalidade'])
                                            echo '<span style="color: green; font-size: 0.75em; font-weight: bold;"><center>Habilitada<center></span>';
                                        else
                                            echo '<span style="color: red; font-size: 0.75em; font-weight: bold;"><center>Desabilitada<center></span>';
                                    echo '</td>';
                                echo '</tr>';
                                ++$i;
                            }
                            ?>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </html>

    <script  type="text/javascript">
        function selecionar(numFuncs)
        {
            if(document.getElementById('selecionaTodas').checked)
            {
                for(i = 0; i < numFuncs; ++i)
                    if(!document.getElementById('cb' + i).checked)
                        document.getElementById('cb' + i).checked = true;
            }
            else
            {
                for(i = 0; i < numFuncs; ++i)
                    if(document.getElementById('cb' + i).checked)
                        document.getElementById('cb' + i).checked = false;
            }
        }

        function habilitar(numFuncs, hab)
        {
            var count = 0;
            var id = new Array();

            for(i = 0; i < numFuncs; ++i)
            {
                if(document.getElementById('cb' + i).checked)
                {
                    id[count] = document.getElementById('cb' + i).value;
                    ++count;
                }
            }

            var link = "?count=" + count + "&hab=" + hab;

            for(i = 0; i < count; ++i) link += "&id" + i + "=" + id[i];

            window.location.href = "habilitar_func.php" + link;
        }
    </script>

<?php
    require 'rodape.php';
}