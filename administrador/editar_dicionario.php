<?php
    require("../phpsqlinfo_dbinfo.php");
    session_start();
    if(!isset($_SESSION['user_admin_'.$link_inicial]) && !isset($_SESSION['pass_admin_'.$link_inicial])){
        header("location: naologado_admin.html");
    }else{
?>

    <html>
        <?php
        $id = '';
        if(isset($_GET['id'])) $id = $_GET['id'];

        //Consultas SQL
        $consulta = $connection->query("SELECT * FROM dicionario WHERE codPalavra = '$id'");
        if(!($linhaDaConsulta = $consulta->fetch_assoc())){
            echo "Erro na consulta : Não foram encontrados os dados na tabela dicionario";
            exit;
        }

        $palavra = $linhaDaConsulta['palavraAbreviada'];
        $subs = $linhaDaConsulta['substituicao'];

        require ("cabecalho.php");
        require ("menu.php");?>

        <div id="toolbar-box">
            <div class="m">
                <div class="toolbar-list" id="toolbar">
                    <ul>
                        <li class="button" id="toolbar-apply">
                        <a href="#" onclick="salvar(<?php echo $id; ?>)" class="toolbar">
                            <span class="icon-32-apply"></span>
                            Salvar
                        </a>
                        </li>

                        <li class="divider"></li>

                        <li class="button" id="toolbar-cancel">
                            <a href="listar_dicionario.php" class="toolbar">
                            <span class="icon-32-cancel"></span>
                            Cancelar
                            </a>
                        </li>
                    </ul>
                    <div class="clr"></div>
                </div>
                <div class="pagetitle icon-48-category-edit"><h2>Editar palavra do dicionário</h2></div>
            </div>
        </div>

        <div id="element-box">
            <div class="m">
                <form action="#" id="application-form" method="post" name="adminForm" class="form-validate">
                    <div id="config-document">
                        <div id="page-site" class="tab" style="display: block;">
                            <div class="noshow">
                                <div class="width-60 fltlft">
                                    <div class="width-100">
                                        <fieldset class="adminform">
                                            <legend>Editar palavra</legend>
                                            <ul class="adminformlist">
                                                <li>
                                                    <label id="jform_novaPalavra-lbl" for="jform_novaPalavra" class="hasTip required" title="" aria-invalid="false">
                                                        Palavra
                                                        <span class="star">&nbsp;*</span>
                                                    </label>
                                                    <input type="text" name="jform[novaPalavra]" id="jform_novaPalavra" value="<?php echo $palavra ?>" size="50" aria-invalid="false">
                                                </li>
                                                <li>
                                                    <label id="jform_subs-lbl" for="jform_subs" class="hasTip required" title="" aria-invalid="false">
                                                        Substituição
                                                        <span class="star">&nbsp;*</span>
                                                    </label>
                                                    <input type="text" name="jform[subs]" id="jform_subs" value="<?php echo $subs ?>" size="50" aria-invalid="false">
                                                </li>
                                            </ul>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="clr"></div>
            </div>
        </div>
    </html>

    <script  type="text/javascript">
        function salvar(id)
        {
            var novaPalavra  = document.getElementById('jform_novaPalavra').value;
            var subs  = document.getElementById('jform_subs').value;

            if(novaPalavra == '' || subs == '')
                alert("Não deixe nenhum campo em branco");
            else
                window.location.href = "edita_dicionario.php?novaPalavra=" + novaPalavra + "&subs=" + subs + "&id=" + id;
        }
    </script>
<?php
    require 'rodape.php';
    }
?>
