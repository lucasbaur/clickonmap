<?php
    require("../phpsqlinfo_dbinfo.php");
    session_start();
    if(!isset($_SESSION['user_admin_'.$link_inicial]) && !isset($_SESSION['pass_admin_'.$link_inicial])){
        header("location: naologado_admin.html");
    }else{
?>

    <html>
        <?php
        require ("cabecalho.php");
        require ("menu.php");?>

        <div id="toolbar-box">
            <div class="m">
                <div class="toolbar-list" id="toolbar">
                    <ul>
                        <li class="button" id="toolbar-apply">
                        <a href="#" onclick="salvar()" class="toolbar">
                            <span class="icon-32-apply"></span>
                            Salvar
                        </a>
                        </li>

                        <li class="button" id="toolbar-save-new">
                            <a href="#" onclick="salvarenovo()" class="toolbar">
                                <span class="icon-32-save-new">
                                </span>
                                Salvar &amp; Nova
                            </a>
                        </li>

                        <li class="divider"></li>

                        <li class="button" id="toolbar-cancel">
                            <a href="listar_dicionario.php" class="toolbar">
                            <span class="icon-32-cancel"></span>
                            Cancelar
                            </a>
                        </li>
                    </ul>
                    <div class="clr"></div>
                </div>
                <div class="pagetitle icon-48-category-add"><h2>Nova palavra do dicionário</h2></div>
            </div>
        </div>

        <div id="element-box">
            <div class="m">
                <form action="#" id="application-form" method="post" name="adminForm" class="form-validate">
                    <div id="config-document">
                        <div id="page-site" class="tab" style="display: block;">
                            <div class="noshow">
                                <div class="width-60 fltlft">
                                    <div class="width-100">
                                        <fieldset class="adminform">
                                            <legend>Nova substituição</legend>
                                            <ul class="adminformlist">
                                                <li>
                                                    <label id="jform_novaPalavra-lbl" for="jform_novaPalavra" class="hasTip required" title="" aria-invalid="false">Palavra
                                                        <span class="star">&nbsp;*</span>
                                                    </label>
                                                    <input type="text" name="jform[novaPalavra]" id="jform_novaPalavra" value="" size="50"  aria-invalid="false">
                                                </li>
                                                <li>
                                                    <label id="jform_subs-lbl" for="jform_subs" class="hasTip required" title="" aria-invalid="false">Substituição
                                                        <span class="star">&nbsp;*</span>
                                                    </label>
                                                    <input type="text" name="jform[subs]" id="jform_subs" value="" size="50"  aria-invalid="false">
                                                </li>
                                            </ul>
                                        </fieldset>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </form>
                <div class="clr"></div>
            </div>
        </div>
    </html>

    <script  type="text/javascript">
        function salvar()
        {
            var novaPalavra  = document.getElementById('jform_novaPalavra').value;
            var subs  = document.getElementById('jform_subs').value;

            if(novaPalavra == '' || subs == '')
                alert("Não deixe nenhum campo em branco");
            else
                window.location.href = "cria_dicionario.php?novaPalavra=" + novaPalavra + "&subs=" + subs + "&origem=salvar";
        }

        function salvarenovo()
        {
            var novaPalavra  = document.getElementById('jform_novaPalavra').value;
            var subs  = document.getElementById('jform_subs').value;

            if(novaPalavra == '' || subs == '')
                alert("Não deixe nenhum campo em branco");
            else
                window.location.href = "cria_dicionario.php?novaPalavra=" + novaPalavra + "&subs=" + subs + "&origem=salvarenovo";
        }
    </script>
<?php
        require 'rodape.php';
    }
?>
