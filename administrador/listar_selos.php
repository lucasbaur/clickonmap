<?php
	require '../phpsqlinfo_dbinfo.php';
	session_start();
	if(!isset($_SESSION['user_admin_' . $link_inicial]) && !isset($_SESSION['pass_admin_' . $link_inicial]))
		header("location: naologado_admin.html");
	else
	{
?>

	<html>
		<?php
			//Consultas SQL
			$consulta = $connection->query("SELECT * FROM selosconquistas");
			$numSelos = $consulta->num_rows;

			require 'cabecalho.php';
			require 'menu.php';
		?>

		<div id="toolbar-box">
			<div class="m">
				<div class="toolbar-list" id="toolbar">
					<ul>
						<li class="button" id="toolbar-edit">
							<a href="#" onclick="editar(<?php echo $numSelos; ?>)" class="toolbar">
								<span class="icon-32-edit"></span>
								Editar
							</a>
						</li>

						<li class="divider"></li>

						<li class="button" id="toolbar-new">
							<a href="#" onclick="habilitar(<?php echo $numSelos; ?>, 1)" class="toolbar">
								<span class="icon-32-publish"></span>
								Habilitar
							</a>
						</li>

						<li class="button" id="toolbar-new">
							<a href="#" onclick="habilitar(<?php echo $numSelos; ?>, 0)" class="toolbar">
								<span class="icon-32-unpublish"></span>
								Desabilitar
							</a>
						</li>
					</ul>
					<div class="clr"></div>
				</div>
				<div class="pagetitle icon-48-selo"><h2>Selos de Conquista</h2></div>
			</div>
		</div>

		<div id="element-box">
			<div class="m">
				<form action="#" id="application-form" method="post" name="adminForm" class="form-validate">
					<table class="adminlist">
						<thead>
							<tr>
								<th width="1%">
									<input type="checkbox" name="selecionaTodas" id="selecionaTodas" value="" title="Selecionar todos os selos" onclick="selecionar(<?php echo $numSelos; ?>)">
								</th>
								<th>Nome</th>
								<th width="20%">Status</th>
							</tr>
						</thead>

						<tbody>
							<?php
							$i = 0;
							while($selo = $consulta->fetch_array())
							{
								echo '<tr class="row' . ($i % 2) . '">';
									echo '<td class="center">';
										echo '<input type="checkbox" id="cb' . $i . '" name="cid[]" value="' . $selo['idSelo'] . '" title="Selecionar esta conquista">';
									echo '</td>';
									echo '<td>';
										echo '<a href="editar_selo.php?id=' . $selo['idSelo'] . '"  style="font-size: 0.85em;">' . $selo['nomeSelo'] . '</a>';
									echo '</td>';

									echo '<td>';
										if ($selo['seloAtivo'])
											echo '<span style="color: green; font-size: 0.75em; font-weight: bold;"><center>Habilitado<center></span>';
										else
											echo '<span style="color: red; font-size: 0.75em; font-weight: bold;"><center>Desabilitado<center></span>';
									echo '</td>';
								echo '</tr>';
								++$i;
							}
							?>
						</tbody>
					</table>
				</form>
			</div>
		</div>
	</html>

	<script  type="text/javascript">
		function selecionar(numSelos)
		{
			if(document.getElementById('selecionaTodas').checked)
			{
				for(i = 0; i < numSelos; ++i)
					if(!document.getElementById('cb' + i).checked)
						document.getElementById('cb' + i).checked = true;
			}
			else
			{
				for(i = 0; i < numSelos; ++i)
					if(document.getElementById('cb' + i).checked)
						document.getElementById('cb' + i).checked = false;
			}
		}

		function editar(numSelos)
		{
			var count = 0;
			var id;

			for(i = 0; i < numSelos; ++i)
			{
				if(document.getElementById('cb' + i).checked)
				{
					++count;
					id = document.getElementById('cb' + i).value;
				}
			}

			if(count == 0) alert("Você deve selecionar um selo para ser editado!");
			else if(count > 1) alert("Você deve selecionar somente um selo para ser editado!");
			else window.location.href = "editar_selo.php?id=" + id;
		}

        function habilitar(numSelos, hab)
        {
            var count = 0;
            var id = new Array();

            for(i = 0; i < numSelos; ++i)
            {
                if(document.getElementById('cb' + i).checked)
                {
                    id[count] = document.getElementById('cb' + i).value;
                    ++count;
                }
            }

            var link = "?count=" + count + "&hab=" + hab;

            for(i = 0; i < count; ++i) link += "&id" + i + "=" + id[i];

            window.location.href = "habilitar_selo.php" + link;
        }
	</script>

<?php
	require 'rodape.php';
}