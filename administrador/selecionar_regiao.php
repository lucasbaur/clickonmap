<?php
    require '../phpsqlinfo_dbinfo.php';
    session_start();
    if(!isset($_SESSION['user_admin_' . $link_inicial]) && !isset($_SESSION['pass_admin_' . $link_inicial]))
        header("location: naologado_admin.html");
    else
    {
?>

    <html>
        <?php
            //Consultas SQL
            $consulta = $connection->query("SELECT codFuncionalidade, nomeFuncionalidade, descFuncionalidade, statusFuncionalidade
                                            FROM funcionalidadestratamentotexto");
            $numFuncs = $consulta->num_rows;

            require 'cabecalho.php';
            require 'menu.php';
        ?>

        <script type="text/javascript" async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAhE0kcLiR1qn0pcUJ5UVCo7xSyBL3VE6Y&libraries=places,visualization"></script>
        <script type="text/javascript" src="../src/jquery.min.js"></script>
        <script type="text/javascript" src="../map.js"></script>
        <script type="text/javascript">
            var map;
            function initialize()
            {
                map = initMap();
            }
        </script>
        <body onload="initialize()">
        <input type="hidden" name="longitude_inicial" id="longitude_inicial" value="-51.92528">
        <input type="hidden" name="latitude_inicial" id="latitude_inicial" value="-14.235004">
        <input type="hidden" name="zoom_inicial" id="zoom_inicial" value="4">
        <input type="hidden" name="tipoMapa_inicial" id="tipoMapa_inicial" value="TERRAIN">
        <div id="toolbar-box">
            <div class="m">
                <div class="toolbar-list" id="toolbar">
                        <li class="button" id="toolbar-new">
                            <a href="#" onclick="habilitar(<?php echo $numFuncs; ?>, 1)" class="toolbar">
                                <span class="icon-32-publish"></span>
                                Salvar região
                            </a>
                        </li>

                        <li class="button" id="toolbar-new">
                            <a href="#" onclick="habilitar(<?php echo $numFuncs; ?>, 0)" class="toolbar">
                                <span class="icon-32-unpublish"></span>
                                Apagar região
                            </a>
                        </li>
                    </ul>
                    <div class="clr"></div>
                </div>
                <div class="pagetitle icon-48-info"><h2>Selecionar região</h2></div>
            </div>
        </div>

        <div id="map_canvas" class="map_canvas" style="width: 80%; height: 500px"></div>
        </body>
    </html>
<?php
    require 'rodape.php';
}