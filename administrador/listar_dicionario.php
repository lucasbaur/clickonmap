<?php
    require '../phpsqlinfo_dbinfo.php';
    session_start();
    if(!isset($_SESSION['user_admin_' . $link_inicial]) && !isset($_SESSION['pass_admin_' . $link_inicial]))
        header("location: naologado_admin.html");
    else
    {
?>

    <html>
        <?php
            //Consultas SQL
            $consulta = $connection->query("SELECT * FROM dicionario");
            $numPalavras = $consulta->num_rows;

            require 'cabecalho.php';
            require 'menu.php';
        ?>

        <div id="toolbar-box">
            <div class="m">
                <div class="toolbar-list" id="toolbar">
                    <ul>
                        <li class="button" id="toolbar-new">
                            <a href="adicionar_dicionario.php" class="toolbar">
                                <span class="icon-32-new"></span>
                                Adicionar
                            </a>
                        </li>

                        <li class="button" id="toolbar-edit">
                            <a href="#" onclick="editar(<?php echo $numPalavras;?>)" class="toolbar">
                                <span class="icon-32-edit"></span>
                                Editar
                            </a>
                        </li>

                        <li class="divider"></li>

                        <li class="button" id="toolbar-trash">
                            <a href="#" onclick="excluir(<?php echo $numPalavras; ?>)" class="toolbar">
                                <span class="icon-32-trash"></span>
                                Excluir
                            </a>
                        </li>
                    </ul>
                    <div class="clr"></div>
                </div>
                <div class="pagetitle icon-48-info-dark"><h2>Palavras do dicionário</h2></div>
            </div>
        </div>

        <div id="element-box">
            <div class="m">
                <form action="#" id="application-form" method="post" name="adminForm" class="form-validate">
                    <table class="adminlist">
                        <thead>
                            <tr>
                                <th width="1%">
                                    <input type="checkbox" name="selecionaTodas" id="selecionaTodas" value="" title="Selecionar todas as palavras" onclick="selecionar(<?php echo $numPalavras; ?>)">
                                </th>
                                <th>Palavra</th>
                                <th>Substituição</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            $i = 0;
                            while($palavra = $consulta->fetch_array())
                            {
                                echo '<tr class="row' . ($i % 2) . '">';
                                    echo '<td class="center">';
                                        echo '<input type="checkbox" id="cb' . $i . '" name="cid[]" value="' . $palavra['codPalavra'] . '" title="Selecionar esta palavra">';
                                    echo '</td>';
                                    echo '<td align="right">';
                                        echo '<a href="editar_dicionario.php?id=' . $palavra['codPalavra'] . '" style="font-size: 0.85em;">' . $palavra['palavraAbreviada'] . '</a>';
                                    echo '</td>';
                                    echo '<td>';
                                        echo '<a href="editar_dicionario.php?id=' . $palavra['codPalavra'] . '" style="font-size: 0.85em;">' . $palavra['substituicao'] . '</a>';
                                    echo '</td>';
                                echo '</tr>';
                                ++$i;
                            }
                            ?>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </html>

    <script  type="text/javascript">
        function selecionar(numPalavras)
        {
            if(document.getElementById('selecionaTodas').checked)
            {
                for(i = 0; i < numPalavras; ++i)
                    if(!document.getElementById('cb' + i).checked)
                        document.getElementById('cb' + i).checked = true;
            }
            else
            {
                for(i = 0; i < numPalavras; ++i)
                    if(document.getElementById('cb' + i).checked)
                        document.getElementById('cb' + i).checked = false;
            }
        }

        function editar(numPalavras)
        {
            var count = 0;
            var id;

            for(i = 0; i < numPalavras; ++i)
            {
                if(document.getElementById('cb' + i).checked)
                {
                    count++;
                    id = document.getElementById('cb' + i).value;
                }
            }

            if(count == 0) alert("Você deve selecionar uma palavra para ser editada!");
            else if(count > 1) alert("Você deve selecionar somente uma palavra para ser editada!");
            else window.location.href = "editar_dicionario.php?id=" + id;
        }

        function excluir(numPalavras)
        {
            var count = 0;
            var id = new Array();

            for(i = 0; i < numPalavras; ++i)
            {
                if(document.getElementById('cb' + i).checked)
                {
                    id[count] = document.getElementById('cb' + i).value;
                    ++count;
                }
            }

            var link = "?count=" + count;

            for(i = 0; i < count; ++i) link += "&id" + i + "=" + id[i];

            if (confirm('Você realmente deseja excluir a(s) palavra(s) selecionado(s)?'))
                window.location.href = "excluir_dicionario.php" + link;
        }
    </script>

<?php
    require 'rodape.php';
}