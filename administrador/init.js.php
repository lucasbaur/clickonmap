<!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css" /> -->
<link href="../style/bootstrap-popover.css" rel="stylesheet" />
<script type="text/javascript" src="../src/jquery.min.js"></script>
<script type="text/javascript" src="../bootstrap/js/tooltip.js"></script>
<script type="text/javascript" src="../bootstrap/js/popover.js"></script>
<script type="text/javascript">
    var popoverShow = true;

    function initialize()
    {
        $('#mobpopover').popover(
        {
            content: 'Clique aqui para disponibilizar seu ClickOnMap para dispositivos mobile',
            container: 'body',
            placement: 'left'
        }).data('bs.popover').tip().attr('id', 'mobpopoverb');
        $('#mobpopover').popover('show');

        $(window).bind('resize', changed())

        $('#mobpopover').change(changed());
    }

    function changed()
    {
        $('#mobpopover').popover('hide');
        if(!$('input[name=mobpopover]').is(':checked'))
        {
            $('#mobpopover').popover('show');
            $('#mobpopover').data("bs.popover").inState.click = true;
        }
        else
        {
            $('#mobpopover').popover('hide');
            <?php
                require("../phpsqlinfo_dbinfo.php");

                if ($connection->query("SELECT * FROM categoriaevento")->num_rows == 0)
                {
            ?>
                    alert("Primeiro insira ao menos uma categoria de colaboração!");
                    setTimeout(function()
                    {
                        $('#mobpopover').prop('checked', false);
                        $('#mobpopover').popover('show');
                    }, 500);
            <?php
                }
                else
                {
            ?>
                    // Entra em contato com o servidor!
                    alert("Este recurso ainda está sendo desenvolvido.");
            <?php
                }
            ?>
        }
    }
</script>