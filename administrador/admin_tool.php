<?php
    session_start();
    require '../phpsqlinfo_dbinfo.php';
    header('content-type: text/html; charset=utf-8');

    if(empty($_SESSION['user_admin_' . $link_inicial]) && empty($_SESSION['pass_admin_' . $link_inicial]))
    {
        header("location: naologado_admin.html");
    }
    else
    {
        //Consultas SQL
        //Achar número total de usuários
        $consulta = "SELECT COUNT(*) AS totalUsuarios FROM usuario";
        $resultado = $connection->query($consulta);
        if (!$resultado)
        {
            die('Consulta inválida: ' . $connection->error);
        }
        $totalUsuario = $resultado->fetch_assoc();

        //Achar número total de colaborações
        $consulta2 = "SELECT COUNT(*) AS totalcolaboracao FROM colaboracao";
        $resultado2 = $connection->query($consulta2);
        if (!$resultado2) die('Invalid consulta2: ' . $connection->error);
        $totalColaboracoes = $resultado2->fetch_assoc();

        ?>
        <html>
            <?php
                require 'cabecalho.php';
                require 'menu.php';
            ?>

            <style type="text/css">
            .switch
            {
                position: relative;
                float: right;
                display: inline-block;
                width: 60px;
                height: 34px;
                margin-top: 25px;
                margin-right: 50px;
            }

            /* Hide default HTML checkbox */
            .switch input
            {
                visibility: hidden;
                float: left;
                margin-top: 9px;
                margin-left: -5px;
            }

            /* The slider */
            .slider
            {
                position: absolute;
                cursor: pointer;
                top: 0; left: 0; right: 0; bottom: 0;
                background-color: #ccc;
                -webkit-transition: .4s;
                transition: .4s;
            }

            .slider:before
            {
                position: absolute;
                content: "";
                height: 26px;
                width: 26px;
                left: 4px;
                bottom: 4px;
                background-color: white;
                -webkit-transition: .4s;
                transition: .4s;
            }

            input:checked + .slider { background-color: #2196F3; }
            input:focus + .slider { box-shadow: 0 0 1px #2196F3; }

            input:checked + .slider:before
            {
                -webkit-transform: translateX(26px);
                -ms-transform: translateX(26px);
                transform: translateX(26px);
            }

            /* Rounded sliders */
            .slider.round { border-radius: 34px; }
            .slider.round:before { border-radius: 50%; }
            </style>

            <?php require 'init.js.php'; ?>
            <body onload="initialize()">
            <div id="content-box">
                <div id="element-box">
                    <!-- Rounded switch -->
                    <label class="switch">
                        <input type="checkbox" id="mobpopover" name="mobpopover" data-toggle="popover" />
                        <div class="slider round"></div>
                    </label>
                    <div class="m">
                        <div class="adminform">
                            <div class="cpanel-left">
                                <div class="cpanel">
                                    <div class="icon-wrapper">
                                        <div class="icon">
                                            <a href="listar_categorias.php">
                                                <img src="images/conteudo/icon-48-category.png" alt="">
                                                    <span>Categorias</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="icon-wrapper">
                                        <div class="icon">
                                            <a href="listar_menu.php">
                                                <img src="images/conteudo/icon-48-levels.png" alt="">
                                                    <span>Gerenciar Menu</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="icon-wrapper">
                                        <div class="icon">
                                            <a href="listar_tipos.php">
                                                <img src="images/conteudo/icon-48-module.png" alt="">
                                                    <span>Tipos</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="icon-wrapper">
                                        <div class="icon">
                                            <a href="listar_selos.php">
                                                <img src="images/conteudo/icon-48-selo.png" alt="">
                                                    <span>Selos</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="icon-wrapper">
                                        <div class="icon">
                                            <a href="listar_palavras.php">
                                                <!-- <img src="images/conteudo/icon-48-info.png" alt=""> -->
                                                <img src="images/conteudo/icon-48-keywords-blue.png" alt="">
                                                    <span>Palavras Chave</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="icon-wrapper">
                                        <div class="icon">
                                            <a href="listar_palavras_negras.php">
                                                <img src="images/conteudo/icon-48-keywords-black.png" alt="">
                                                    <span>Lista Negra de Palavras</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="icon-wrapper">
                                        <div class="icon">
                                            <a href="adicionar_usuario.php">
                                                <img src="images/conteudo/icon-48-user-add.png" alt="">
                                                    <span>Adicionar Usuário</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="icon-wrapper">
                                        <div class="icon">
                                            <a href="configuracoes.php">
                                                <img src="images/conteudo/icon-48-config.png" alt="">
                                                    <span>Configurações</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="icon-wrapper">
                                        <div class="icon">
                                            <a href="listar_colaboracoes.php">
                                                <img src="images/conteudo/icon-48-marker.png" alt="">
                                                    <span>Colaborações</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="icon-wrapper">
                                        <div class="icon">
                                            <a href="listar_funcionalidades.php">
                                                <img src="images/conteudo/icon-48-grammar.png" alt="">
                                                    <span>Tratamento de texto</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="icon-wrapper">
                                        <div class="icon">
                                            <a href="selecionar_regiao.php">
                                                <img src="images/conteudo/icon-48-frontpage.png" alt="">
                                                    <span>Selecionar região</span>
                                                    <span>(Mobile - 30% Pronto)</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="icon-wrapper">
                                        <div class="icon">
                                            <a href="http://www.dpi.ufv.br/projetos/clickonmap/">
                                                <img src="images/conteudo/icon-48-site.png" alt="">
                                                    <span>Site ClickOnMap</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cpanel-right">

                                <div id="panel-sliders" class="pane-sliders">
                                    <div style="display:none;"><div></div></div>
                                    <div style="margin-top: 80px; margin-left: 50px;">
                                        <h3>Total de Usuários cadastrados no sistema: <?php echo $totalUsuario['totalUsuarios']; ?></h3>
                                        <h3>Total de colaborações realizadas: <?php echo $totalColaboracoes['totalcolaboracao']; ?></h3>
                                    </div>
                                </div>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </div>
                </div>
            </div>
            </body>
        </html>
    <?php
        require 'rodape.php';
    }