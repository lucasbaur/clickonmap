<?php

use Illuminate\Http\Request;
use Controllers\ConfiguracoesController;
use Controllers\UsuarioController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('', ['as' => 'usuario.validaracesso', 'uses' => 'ConfiguracoesController@verificaConexaoBd']);

Route::get('/RetornarConfiguracoes', 'ConfiguracoesController@index');

Route::group(['prefix' => '/Usuario'], function () {
    Route::post('/Incluir', ['as' => 'usuario.store', 'uses' => 'UsuarioController@store']);
    Route::get('/ValidarAcesso', ['as' => 'usuario.validaracesso', 'uses' => 'UsuarioController@validaracesso']);
    Route::get('/ValidarAcessoAnonimo', ['as' => 'usuario.validaracessoanonimo', 'uses' => 'UsuarioController@validaracessoanonimo']);
});    

Route::group(['prefix' => '/Colaboracao'], function () {
    Route::get('/RetornarColaboracoes', ['as' => 'colaboracao.colaboracoes', 'uses' => 'ColaboracaoController@getColaboracoes']);
    Route::get('/RetornarCategorias', ['as' => 'colaboracao.categoria', 'uses' => 'ColaboracaoController@getCategorias']);
    Route::get('/RetornarTipos', ['as' => 'colaboracao.tipo', 'uses' => 'ColaboracaoController@getTipos']);
    Route::post('/Incluir', ['as' => 'colaboracao.store', 'uses' => 'ColaboracaoController@store']);
    Route::post('/IncluirFoto', ['as' => 'colaboracao.foto', 'uses' => 'ColaboracaoController@storeFoto']);
});

Route::fallback(function(){
    return response()->json([
        'status' => 400,
        'message' => 'Solicitacao nao encontrada'], 400);
});