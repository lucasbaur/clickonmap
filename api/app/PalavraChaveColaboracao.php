<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PalavraChaveColaboracao extends Model
{
    protected $table = "palavraschavecolaboracoes";

    public $timestamps = false;
    
}
