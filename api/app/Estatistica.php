<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estatistica extends Model
{
    protected $table = "estatistica";

    public $timestamps = false;
    
}
