<?php

namespace App\Http\Controllers;
use DB;

class FiltroGramaticalController extends Controller
{
###########################################
# Paulo Collares Moreira Neto
# 27 de dezembro de 2011
# www.paulocollares.com.br
###########################################
# Modificado por Rafael Oliveira Pereira
# Ao longo do ano de 2016

// Esta Função transforma o texto em minúsculo respeitando a acentuação
public function str_minuscula($texto)
{
    //$texto = strtr(strtolower($texto),"ÀÁÂÃÇÈÉÊÌÍÎÑÒÓÔÕÙÜÚÇ","àáâãçèéêìíîñòóôõùüúç");
    $texto = mb_convert_case($texto, MB_CASE_LOWER, "UTF-8");
    return $texto;
}

// Esta Função transforma o texto em maiúsculo respeitando a acentuação
public function str_maiuscula($texto)
{
    $texto = strtr(strtoupper($texto),"àáâãçèéêìíîñòóôõùüúç","ÀÁÂÃÇÈÉÊÌÍÎÑÒÓÔÕÙÜÚÇ");
    return $texto;
}

// Esta Função transforma a primeira letra do texto em maiúsculo respeitando a acentuação
public function primeira_maiuscula($texto)
{
    $texto = strtr(ucfirst($texto),"ÀÁÂÃÇÈÉÊÌÍÎÑÒÓÔÕÙÜÚÇ","àáâãçèéêìíîñòóôõùüúç");
    return $texto;
}

// Verifica se a palavra está toda em maiúscula
public function comparaPalavraMaiuscula($palavra)
{

    $palavra = str_replace(" ", "",$palavra);

    if ($palavra == "") return false;
    if ($palavra == "[:p:]")  return false;
    if (strlen($palavra) <= 1) return false;

    $palavra = preg_replace("[^a-zA-Z0-9]", "", strtr($palavra, "áàãâéêíóôõúçÁÀÃÂÉÊÍÓÔÕÚÇ ", "aaaaeeioooucAAAAEEIOOOUC_"));

    return ($palavra == $this->str_maiuscula($palavra));
}


/////////////////////////////////
// Filtro
/////////////////////////////////

public function filtro($texto, $ponto_final = true)
{
    // Variáveis (Nao pode colocar ponto final senao vai tirar reticencias
    $pontuacoes = array("[\s]?\, ", "[\s]?\!", "[\s]?\?", "[\s]?\;"); // Como expressao regular, remove espacos entre as pontuacoes

    $padrao = '([^\p{L}])'; // Qualquer caracter que nao seja letras maiusculas e nem minusculas

    $dicionario = array();

    $query = DB::table('dicionario')->get(['palavraAbreviada','substituicao']);

    foreach($query as $row) {
        $dicionario[$row->palavraAbreviada] = $row->substituicao;
    }

    $listanegra = false;
    $correcao = false;
    $excessoespaco = false;
    $excessoletras = false;
    $capitalizacao = false;
    $espacamento = false;
    $parenteses = false;
    $paragrafos = false;

    $query = DB::table('funcionalidadestratamentotexto')->get(['nomeFuncionalidade','statusFuncionalidade']);

    foreach($query as $row) {
        if ($row->nomeFuncionalidade == "correcao") $correcao = $row->statusFuncionalidade;
        else if ($row->nomeFuncionalidade == "listanegra") $listanegra = $row->statusFuncionalidade;
        else if ($row->nomeFuncionalidade == "excessoespaco") $excessoespaco = $row->statusFuncionalidade;
        else if ($row->nomeFuncionalidade == "excessoletras") $excessoletras = $row->statusFuncionalidade;
        else if ($row->nomeFuncionalidade == "capitalizacao") $capitalizacao = $row->statusFuncionalidade;
        else if ($row->nomeFuncionalidade == "espacamento") $espacamento = $row->statusFuncionalidade;
        else if ($row->nomeFuncionalidade == "parenteses") $parenteses = $row->statusFuncionalidade;
        else if ($row->nomeFuncionalidade == "paragrafos") $paragrafos = $row->statusFuncionalidade;
    }

    if ($listanegra)
    {
        $query = DB::table('listanegra')->get(['palavra']);
        foreach($query as $row) {
            $dicionario[$row->palavra] = "";
        }
    }

    // Remove o excesso de espaços em branco -> amanha    vou falar            com vc => amanha vou falar com vc
    if ($excessoespaco)
    {
        $texto = preg_replace("/\s(?=\s)/", "", $texto);
    }

    // Remove letras repetidas
    if ($excessoletras)
    {
        $num_repeticao_permitida = 2;   // Quantas repeticoes de uma letra é permitida, min 2 senao remove rr ss...
        for ($i = 0; $i < strlen($texto); $i++)
        {
            $sequencia = '';
            $sequencia .= $texto[$i];
            for ($j = $i+1; $j < strlen($texto); ++$j)
            {
                if ($texto[$i] == $texto[$j]) $sequencia .= $texto[$j];
                else
                {
                    if(strlen($sequencia) > $num_repeticao_permitida && !in_array($sequencia, array('k')))
                        $texto = str_replace($sequencia, $sequencia[0], $texto);
                    break;
                }
            }
        }
    }


    // Expandir palavras abreviadas
    if ($correcao)
    {
        $palavras_erradas = array();
        $palavras_certas = array();

        foreach ($dicionario as $errado => $certo)
        {
            //'/'.$padrao.'errado'.$padrao.'/' => '\1certo\2',
            array_push($palavras_erradas, '/'.$padrao.$errado.$padrao.'/');
            array_push($palavras_certas, '\1'.$certo.'\2');
        }

        $texto = ' '.$texto.' '; // Adiciona espaco antes e depois do texto para substituir abreviacao no inicio e no final caso tenha
        $texto = preg_replace($palavras_erradas, $palavras_certas, $this->str_minuscula($texto)); // Substituir palavras do dicionario
        $texto = substr($texto, 1, strlen($texto) - 1); // Remove espacos em branco adicionado antes das substituicoes
    }

    foreach ($pontuacoes as $pontuacao)
        $texto = preg_replace("/" . $pontuacao . "(?=" . $pontuacao . ")/", "", $texto);

    // Prepara paragrafo
    if ($paragrafos) $texto = str_replace("", "[:p:]", $texto);

    // Acerta maiúscula e minúscula e inicia as sentenças com a primeira letra maiúscula
    if($capitalizacao)
    {
        $array = explode(" ", $texto);
        $novo_texto = "";
        $tam_array = count($array);

        for ($i = 0; $i < $tam_array; ++$i)
        {
            $palavra = $array[$i];

            if ($this->comparaPalavraMaiuscula($palavra)) $nova_palavra = $this->str_minuscula($palavra);
            else $nova_palavra = $palavra;

            $caracter_anterior = ($i > 0 ? substr($array[$i - 1], -1) : ' ');
            $caracter_anterior_paragrafo = ($i > 0 ? substr($array[$i - 1], -5) : ' ');

            if (in_array($caracter_anterior, array('.', '!', '?')) || $i == 0)
                $nova_palavra = $this->primeira_maiuscula($nova_palavra);


            $novo_texto .= $nova_palavra . " ";
        }

        $texto = $novo_texto;
    }

    // Adiciona espaços depois das pontuações e remove antes
    if ($espacamento)
    {
        for ($i = 0; $i < count($pontuacoes); ++$i)
        {
            $ponto = $pontuacoes[$i];
            $texto = str_replace(" " . $ponto . " ",$ponto . " ",$texto);
            $texto = str_replace(" " . $ponto,$ponto . " ",$texto);
            $texto = str_replace($ponto,$ponto . " ",$texto);
        }
    }

    // Acerta parênteses
    if($parenteses)
    {
        $texto = str_replace(" ( ", " (", $texto);
        $texto = str_replace("( ", " (", $texto);
        $texto = str_replace("(", " (", $texto);
        $texto = str_replace(" ) ", ") ", $texto);
        $texto = str_replace(" )", ") ", $texto);
        $texto = str_replace(")", ") ", $texto);
    }

    // Adicionar paragrafo
    if (isset($paragrafo))
    {
        $texto = str_replace("\n", "", $texto);
        $texto = str_replace("\r", "", $texto);

        for ($i = 0; $i <= 10; ++$i)
            $texto = str_replace("[:p:][:p:]", "[:p:]", $texto);

        $array = explode("[:p:]", $texto);
        $novo_texto = "";
        $tam_array = count($array);
        for ($i = 0; $i < $tam_array; ++$i)
        {
            $paragrafo = $array[$i];

            $paragrafo = trim($paragrafo);
            $paragrafo = trim($paragrafo, ",");
            $paragrafo = $this->primeira_maiuscula($paragrafo);

            if ($paragrafo == "") break;

            $ultimo_caracter = substr($paragrafo, -1);

            if ($ultimo_caracter != '.' && $ultimo_caracter != '!' && $ultimo_caracter != '?' && $ultimo_caracter != ':' && $ultimo_caracter != ';' && $ponto_final)
                $paragrafo .= ".";

            if ($i != $tam_array)
                $novo_texto .= $paragrafo . "";
        }

        $texto = $novo_texto;
    }

    return $texto;
}

}