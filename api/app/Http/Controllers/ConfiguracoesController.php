<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Configuracoes;

use DB;
Use Exception;

class ConfiguracoesController extends Controller
{
    public function index()
    {
        return Configuracoes::get(['nomeSite', 'longitude', 'latitude', 'zoom', 'tipoMapa', 'loginFacebook', 'appIDFacebook', 'appSecretFacebook', 'loginGoogle', 'loginAnonimo', 'linkBase']);
    }

    public function verificaConexaoBd()
    {
        try
        {
            DB::connection()->getPdo();
            return response()->json(['status' => 200, 'mensagem' => "Conectado"]);
        }
        catch(Exception $e)
        {
            return response()->json(['status' => 204, 'mensagem' =>  "Desconectado - ".$e->getMessage()]);
          ;
        }
    }

}