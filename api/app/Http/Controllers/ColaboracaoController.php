<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Colaboracao;
use App\Categoria;
use App\Tipo;
use App\UsuarioBloqueado;
use App\PalavraChave;
use App\PalavraChaveColaboracao;
use App\HistoricoColaboracao;
use App\Estatistica;
use App\Usuario;
use App\Multimidia;
use Illuminate\Support\Facades\Validator;

use Illuminate\Contracts\Filesystem\Factory;

use Illuminate\Support\Facades\Storage;

use DB;

class ColaboracaoController extends Controller
{
    private function getFiltroGramatical()
	{
		return app()->make('App\Http\Controllers\FiltroGramaticalController');
    }

    public function getColaboracoes()
    {
        $colaboracoesBD = Colaboracao::where('tipoStatus','A')->get();
        // dd($colaboracoesBD);
        $colaboracoes = collect();
        foreach ($colaboracoesBD as $colaboracao) {
            $obj = new class{};
            $obj->codigoColaboracao = $colaboracao->codColaboracao;
            $obj->titulo = $colaboracao->desTituloAssunto;
            $obj->descricao = $colaboracao->desColaboracao;
            $obj->dataHoraCriacao = $colaboracao->datahoraCriacao;
            $obj->codigoCategoria = $colaboracao->codCategoriaEvento;
            $obj->nomeCategoria = $colaboracao->nomeCategoria;
            $obj->codigoTipo = $colaboracao->codTipoEvento;
            $obj->nomeTipo = $colaboracao->nomeTipo;
            $obj->codigoUsuario = $colaboracao->codUsuario;
            $obj->apelidoUsuario = $colaboracao->apelidoUsuario;
            $obj->dataHoraOcorrencia = $colaboracao->dataHoraOcorrencia;
            $obj->latitude = $colaboracao->numLatitude;
            $obj->longitude = $colaboracao->numLongitude;
            $obj->status = $colaboracao->tipoStatus;
            $obj->zoom = $colaboracao->zoom;
            $obj->ip = $colaboracao->ip;
            $colaboracoes->push($obj);
        }


        return response()->json([
            'status' => 200,
            'listaColaboracoes' => $colaboracoes
        ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'titulo'     => 'required|max:100',
            'descricao'      => 'required|max:100',
            'codigoCategoria'   => 'required',
            'codigoTipo'    => 'required',
            'codigoUsuario'  => 'required',
            'dataHoraOcorrencia'  => 'required',
            'latitude'  => 'required',
            'longitude'  => 'required',
            'zoom'  => 'required|max:11',
            'ip'  => 'required',
            'keywords'  => '',
        ]);
        
        if ($validator->fails()) { // 1 - validar dados
            return response()->json([
                'status' => 400,
                'mensagem' => $validator->errors()->all()
            ], 400,  ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }

        if($this->verificaUsuarioBloqueado($request->codigoUsuario))// 2 - verificar se usuário está bloqueado para realizar colaboracoes
        {
            if($this->verificaPontoUnico($request->latitude, $request->longitude)){ // 3 - teste ponto único (bugado)
               
                $colaboracao = new Colaboracao;
                $colaboracao->desTituloAssuntoSemFiltro = $request->titulo;
                $colaboracao->desTituloAssunto = $this->getFiltroGramatical()->filtro($request->titulo);
                $colaboracao->desColaboracaoSemFiltro = $request->descricao;
                $colaboracao->desColaboracao = $this->getFiltroGramatical()->filtro($request->descricao);
                $colaboracao->datahoraCriacao = date("Y-m-d H:i:s");
                $colaboracao->codCategoriaEvento = $request->codigoCategoria;
                $colaboracao->codTipoEvento = $request->codigoTipo?$request->codigoTipo:null;
                $colaboracao->codUsuario = $request->codigoUsuario;
                $colaboracao->dataHoraOcorrencia = $request->dataHoraOcorrencia;
                $colaboracao->numLatitude = floatval($request->latitude);
                $colaboracao->numLongitude = floatval($request->longitude);
                $colaboracao->tipoStatus = "E";
                $colaboracao->zoom = $request->zoom;
                $colaboracao->desJustificativa = "";
                $colaboracao->ip = $request->ip;
                $this->storeLogApi($colaboracao);
                $colaboracao->save();

                $codColaboracao = $colaboracao->id;

                $this->ganhaPontos(10, $request->codigoUsuario);
                $this->keywords($request->keywords, $codColaboracao);
                $this->inserirHistoricoColaboracoes($colaboracao);
                $this->inserirEstatistica($codColaboracao);

                return response()->json([
                    'status' => 201,
                    'mensagem' => 'Colaboração incluída com sucesso!', 
                    'codigoColaboracao' => $codColaboracao 
                ], 201,  ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);

            }else{
                return response()->json([
                    'status' => 400,
                    'mensagem' => 'Não é possível coloborar neste ponto, ja existe uma colaboração'
                ], 400,  ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
            }
        }else{
            return response()->json([
                'status' => 400,
                'mensagem' => 'Usuário bloqueado temporariamente para fazer colaborações'
            ], 400,  ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function getCategorias()
    {
        return response()->json([
            'listaCategorias' => Categoria::get(['codCategoriaEvento as codigo', 'desCategoriaEvento as descricao'
            ])
        ], 201,  ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
    }    
    
    public function getTipos(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'codigoCategoria'     => 'required'
        ]);
        
        if ($validator->fails()) {
            return response()->json(['mensagem' => $validator->errors()->all()], 400,  ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }

        return response()->json([
            'listaTipos' => Tipo::where('codCategoriaEvento',$request->codigoCategoria )->get(['codTipoEvento as codigo', 'desTipoEvento as descricao', 'codCategoriaEvento as codigoCategoria'])
        ], 201,  ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
    }


    public function verificaUsuarioBloqueado($codigoUsuario)
    {
        $usuarioBloqueado = UsuarioBloqueado::where('idUsuario', $codigoUsuario)->orderBy('inicioBloqueio', 'DESC')->first();
        if($usuarioBloqueado){ // se existir registros do usuário
            return $usuarioBloqueado->inicioBloqueio <= date('Y-m-d H:i:s',strtotime('-'.$usuarioBloqueado->tempoBloqueio.' minutes'))?true:false;
        }else{
            return true;
        }
    }

    public function verificaPontoUnico($lat, $long)
    {
        // dd(floatval($lat), floatval($long));
        // return Colaboracao::where('numLatitude', $lat)->where('numLongitude', $long)->count()?true:false;
        return Colaboracao::where('numLatitude', $lat)->where('numLongitude', $long)->count()>=1?false:true;
    }

    public function ganhaPontos($pontos, $codUsuario)
    {
        $pontos_usuario = Usuario::where('codUsuario', $codUsuario)->first()->pontos;
        $pontos_novos = intval($pontos_usuario) + intval($pontos);
        Usuario::where('codUsuario', $codUsuario)->update(['pontos' => $pontos_novos ]);
        $this->atualizaClasse($codUsuario);
    }


    public function atualizaClasse($codUsuario)
	{
		$consulta =  Usuario::where('codUsuario', $codUsuario)->get();
        foreach($consulta as $row) {
            $resultado = $row->pontos;
			if ($resultado < 0) $this->updateClassUsuario(1, $codUsuario);
			else if ( $resultado >= 0 && $resultado < 100) $this->updateClassUsuario(2, $codUsuario);
			else if ( $resultado >= 100 && $resultado < 249) $this->updateClassUsuario(3, $codUsuario);
			else if ( $resultado >= 250 && $resultado < 449) $this->updateClassUsuario(4, $codUsuario);
			else if ( $resultado >= 500 && $resultado < 799) $this->updateClassUsuario(5, $codUsuario);
			else $this->updateClassUsuario(6, $codUsuario);
        }
	}

	public function updateClassUsuario($valor, $codUsuario)
	{
		Usuario::where('codUsuario', $codUsuario)->update(['classeUsuario' => $valor]);
	}

    public function keywords($keywords, $codColaboracao)
    {
        $palavras_chave = explode(';', $keywords);
        foreach ($palavras_chave as $palavra){
            if ($palavra != '') {

                $palavraChave = new PalavraChave(); 
                $palavraChave->descPalavraChave = $palavra; 
                $palavraChave->save(); 

                $codigo_palavra = $palavraChave->id;

                $palavraChaveColaboracao = new PalavraChaveColaboracao();
                $palavraChaveColaboracao->codColaboracao = $codColaboracao;
                $palavraChaveColaboracao->codPalavraChave = $codigo_palavra;
                $palavraChaveColaboracao->save();

            }
        }
    }

    public function inserirHistoricoColaboracoes($colaboracao)
    {

        $historicoColaboracao = new HistoricoColaboracao();
        $historicoColaboracao->codColaboracao = $colaboracao->id;
        $historicoColaboracao->desTituloSemFiltro = $colaboracao->desTituloAssuntoSemFiltro;
        $historicoColaboracao->desTitulo = $colaboracao->desTituloAssunto;
        $historicoColaboracao->desColaboracaoSemFiltro = $colaboracao->desColaboracaoSemFiltro;
        $historicoColaboracao->desColaboracao = $colaboracao->desColaboracao;
        $historicoColaboracao->datahoraModificacao = $colaboracao->datahoraCriacao;
        $historicoColaboracao->codUsuario = $colaboracao->codUsuario;
        $historicoColaboracao->save();
    }

    public function inserirEstatistica($codColaboracao)
    {
        $estatistica = new Estatistica();
        $estatistica->codColaboracao = $codColaboracao;
        $estatistica->qtdVisualizacao = 0;
        $estatistica->qtdAvaliacao = 0; 
        $estatistica->notaMedia = 0;
        $estatistica->pesoTotal = 0;
        $estatistica->save();
    }

    public function storeFoto(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'codigoColaboracao'     => 'required',
            'foto'    => 'required',
            'extensao'    => 'required',
            'titulo'      => 'required|max:100',
            'comentario'   => '',
            'codigoUsuario'  => 'required',
        ]);
        
        if ($validator->fails()) { // 1 - validar dados
            return response()->json([
                'status' => 400,
                'mensagem' => $validator->errors()->all()
            ], 400,  ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
        // if(strpos($request->foto, ';base64')){
        if( base64_encode(base64_decode($request->foto, true)) === $request->foto){
            $base64 = $request->foto;
            //obtem a extensão
            // $extension = explode('/', $base64);
            // $extension = explode(';', $extension[1]);
            // $extension = '.'.$extension[0];
            //gera o nome
            $name = time().'.'.$request->extensao;
            //obtem o arquivo
            // $separatorFile = explode(',', $base64);
            // $file = $separatorFile[1];

            $path = "/";

            $multimidia = new Multimidia();
            $multimidia->desTituloImagemSemFiltro = $request->titulo;
            $multimidia->desTituloImagem = $this->getFiltroGramatical()->filtro($request->titulo);
            $multimidia->comentarioImagemSemFiltro = $request->comentario; 
            $multimidia->comentarioImagem = $this->getFiltroGramatical()->filtro($request->comentario);
            $multimidia->endImagem = $name;
            $multimidia->codColaboracao = $request->codigoColaboracao;
            $multimidia->codUsuario = $request->codigoUsuario;
            $multimidia->dataEnvioImagem = date("Y-m-d H:i:s");

            $multimidia->save();


            Storage::disk('imagensenviadas')->put($path.$name, base64_decode($request->foto));

            return response()->json([
                'status' => 201,
                'mensagem' => 'Arquivo enviado com sucesso'
            ], 201);
            
        }else{
            return response()->json([
                'status' => 400,
                'mensagem' => 'Envie a foto no formato base64']
                , 422);
        }
    }

    public function storeLogApi($dados)
    {
        Storage::delete('api_log.txt');
        Storage::disk('local')->put('api_log.txt', $dados);
    }

}