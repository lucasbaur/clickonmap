<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use Illuminate\Support\Facades\Validator;

class UsuarioController extends Controller
{

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nome'     => 'required|max:100',
            'apelido'      => 'required|max:50',
            'faixaEtaria'   => 'required',
            'email'    => 'required|email|max:100',
            'senha'  => 'required|max:50',
        ]);
        
        if ($validator->fails()) {
            return response()->json(['mensagem' => $validator->errors()->all()], 400,  ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
        
        if($this->verificaExisteApelidoUsuario($request->apelido)){
            return response()->json(['mensagem' => 'Apelido já utilizado.'], 400,  ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
        if($this->verificaExisteEmailUsuario($request->email)){
            return response()->json(['mensagem' => 'Email já utilizado.'], 400,  ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }

        $usuario = new Usuario;
        $usuario->apelidoUsuario = $request->apelido;
        $usuario->nomPessoa = $request->nome;
        $usuario->endEmail = $request->email;
        $usuario->senha = md5($request->senha);
        $usuario->tipoUsuario = "C";
        $usuario->pontos = 5;
        $usuario->faixaEtaria = $request->faixaEtaria;
        $usuario->classeUsuario = 2;
        $usuario->save();
        return response()->json([
            'status' => 201,
            'mensagem' => 'Usuário Cadastrado com sucesso!',
            'dadosUsuario' =>
            Usuario::where("apelidoUsuario",$usuario->apelidoUsuario)->first([
            'codUsuario as codigo',
            'apelidoUsuario as apelido',
            'nomPessoa as nome',
            'endEmail as email',
            'tipoUsuario',
            'pontos',
            'faixaEtaria',
            'classeUsuario',
            'dataCadastro'
            ])
        ], 201,  ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
    }

    public function verificaExisteApelidoUsuario($apelido)
    {
        return Usuario::where("apelidoUsuario",$apelido)->count()>=1?true:false;
    }

    public function verificaExisteEmailUsuario($email)
    {
        return Usuario::where("endEmail",$email)->count()>=1?true:false;
    }

    public function validaracesso(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'emailOuApelido'     => 'required|max:100',
            'senha'      => 'required|max:50'
        ]);
        
        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'mensagem' => $validator->errors()->all()
        ], 400,  ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }

        if($this->login($request->emailOuApelido, $request->senha)){
            return response()->json([
                'status' => 200,
                'mensagem' => 'Dados validados com sucesso!',
                'dadosUsuario' =>
                    Usuario::where("endEmail",$request->emailOuApelido)->orWhere("apelidoUsuario",$request->emailOuApelido)->first([
                    'codUsuario as codigo',
                    'apelidoUsuario as apelido',
                    'nomPessoa as nome',
                    'endEmail as email',
                    'tipoUsuario',
                    'pontos',
                    'faixaEtaria',
                    'classeUsuario',
                    'dataCadastro'
                    ])
        
                ], 201,  ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }else{
            return response()->json([
                'status' => 400,
                'mensagem' => 'O login ou senha não foram encontrados.'
            ], 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
        
    }

    public function validaracessoanonimo()
    {
        $anonimo = Usuario::count() + 1;
        $usuario = new Usuario;
        $usuario->apelidoUsuario = 'Anonimo'.$anonimo ;
        $usuario->nomPessoa = 'Anonimo'.$anonimo;
        $usuario->endEmail = 'anonimo'.$anonimo.'@anonimo.com.br';
        $usuario->senha = md5('anonimo123456');
        $usuario->tipoUsuario = "C";
        $usuario->pontos = 5;
        $usuario->faixaEtaria = '26 - 64';
        $usuario->classeUsuario = 2;
        $usuario->save();

        if($this->login($usuario->endEmail, "anonimo123456")){
            return response()->json([
                'status' => 200,
                'mensagem' => 'Dados validados com sucesso!',
                'dadosUsuario' =>
                    Usuario::where("endEmail",$usuario->endEmail)->first([
                    'codUsuario as codigo',
                    'apelidoUsuario as apelido',
                    'nomPessoa as nome',
                    'endEmail as email',
                    'tipoUsuario',
                    'pontos',
                    'faixaEtaria',
                    'classeUsuario',
                    'dataCadastro'
                    ])
        
                ], 201, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'] , JSON_UNESCAPED_UNICODE);
        }
    }

    public function login($emailOuApelido, $senha)
    {
        if($this->valida_email($emailOuApelido)){
            return Usuario::where("endEmail",$emailOuApelido)->where('senha',md5($senha ))->count()>=1?true:false;
        }else{
            return Usuario::where("apelidoUsuario",$emailOuApelido)->where('senha',md5($senha))->count()>=1?true:false;
        }
    }

    public function valida_email($email) {
        return preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/', $email);
    }
}