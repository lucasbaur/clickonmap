<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PalavraChave extends Model
{
    protected $table = "palavraschave";

    public $timestamps = false;
    
}
