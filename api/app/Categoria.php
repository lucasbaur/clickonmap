<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = "categoriaevento";
    
    protected $fillable = ['codCategoriaEvento','desCategoriaEvento'];

    public $timestamps = false;
    
}
