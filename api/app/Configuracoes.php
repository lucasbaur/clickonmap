<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configuracoes extends Model
{
    protected $table = "configuracoes";
    
    protected $fillable = ['nomeSite', 'longitude', 'latitude', 'zoom', 'tipoMapa', 'loginFacebook', 'appIDFacebook', 'appSecretFacebook', 'loginGoogle', 'loginAnonimo', 'emailContato', 'senhaContato', 'linkBase'];

    public $timestamps = false;
    
}
