<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = "usuario";
    
    protected $fillable = ['codUsuario', 'apelidoUsuario', 'nomPessoa', 'endEmail', 'senha', 'tipoUsuario', 'pontos', 'faixaEtaria', 'classeUsuario', 'dataCadastro', 'viuSistemaAjuda'];

    public $timestamps = false;
    
}
