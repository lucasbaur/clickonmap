<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo extends Model
{
    protected $table = "tipoevento";
    
    protected $fillable = ['codTipoEvento', 'desTipoEvento', 'codCategoriaEvento'];

    public $timestamps = false;
    
}
