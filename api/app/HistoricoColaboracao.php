<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoricoColaboracao extends Model
{
    protected $table = "historicocolaboracoes";

    public $timestamps = false;
    
}
