<?php

namespace App;

use App\Categoria;
use App\Tipo;
use App\Usuario;

use Illuminate\Database\Eloquent\Model;

class Colaboracao extends Model
{
    protected $table = "colaboracao";
    
    protected $fillable = ['codColaboracao', 'desTituloAssuntoSemFiltro', 'desTituloAssunto', 'desColaboracaoSemFiltro', 'desColaboracao', 'datahoraCriacao', 'codCategoriaEvento', 'codTipoEvento', 'codUsuario', 'dataHoraOcorrencia', 'numLatitude', 'numLongitude', 'tipoStatus', 'desJustificativa', 'zoom', 'ip'];

    public $timestamps = false;

    protected $appends = [
        'nomeCategoria',
        'nomeTipo',
        'apelidoUsuario'
    ];

    public function getNomeCategoriaAttribute(){
        return Categoria::where('codCategoriaEvento',$this->codCategoriaEvento)->first()->desCategoriaEvento;
    }    

    public function getNomeTipoAttribute(){
        return Tipo::where('codTipoEvento',$this->codTipoEvento)->first()->desTipoEvento;
    }   

    public function getApelidoUsuarioAttribute(){
        return Usuario::where('codUsuario',$this->codUsuario)->first()->apelidoUsuario;
    }



    
}
