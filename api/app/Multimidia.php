<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Multimidia extends Model
{
    protected $table = "multimidia";

    public $timestamps = false;
    
}
