<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuarioBloqueado extends Model
{
    protected $table = "usuariosbloqueados";

    public $timestamps = false;
    
}
