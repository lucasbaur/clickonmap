<?php
    //error_reporting(0);//Oculta mensagens de erro, aviso e etc

function createHead($config)
{
	?>
	<head>
		<meta charset='utf-8'>
		<title><?= $config["title"] ?></title>
		<script type="text/javascript" src="links.js"></script>
        <script type="text/javascript" src="src/jquery-ui-1.11.4.custom/external/jquery/jquery.js"></script>
        <script type="text/javascript" src="src/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
        <script type="text/javascript" src="bootstrap/js/social-button.js"></script>
        <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="bootstrap/js/bootbox.min.js"></script>
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="bootstrap/css/social-button.css" type="text/css" />
		<link rel="stylesheet" href="config.css" type="text/css" />
		<link rel="stylesheet" href="style/header.css" type="text/css" />

		<?php
		if (!empty($config["css"]))
			foreach($config["css"] as $confCss)
				echo "<link rel=\"stylesheet\" href=\"$confCss\" type=\"text/css\" />";

		if (!empty($config["script"]))
			foreach($config["script"] as $confScript)
				echo "<script type=\"text/javascript\" src=\"$confScript\"></script>";

		if (!empty($config["required"]))
			foreach($config["required"] as $confReq)
				require $confReq;
		?>

		<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
		<link rel="stylesheet" href="src/jquery-ui-1.11.4.custom/jquery-ui.min.css" />
	</head>

	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-41078245-1', 'ufv.br');
		ga('send', 'pageview');
	</script>

	<?php
}