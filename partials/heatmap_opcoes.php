<div id='opcoes-heatmap' class="row" style='display: none;margin-bottom: 10px;'>
    <button type="button" class="btn btn-warning active" style="margin: 0px 7px;" onclick="changeGradient()"><strong> <span class="glyphicon glyphicon-adjust"></span> Alterar Cores</strong></button>
    <button type="button" class="btn btn-warning active" style="margin: 0px 7px;" onclick="changeRadiusAdd()"><strong> <span class="glyphicon glyphicon-plus-sign"></span> Aumentar Raio</strong></button>
    <button type="button" class="btn btn-warning active" style="margin: 0px 7px;" onclick="changeRadiusSub()"><strong> <span class="glyphicon glyphicon-minus-sign"></span> Diminuir Raio</strong></button>
    <button type="button" class="btn btn-warning active" style="margin: 0px 7px;" onclick="changeOpacity()"><strong> <span class=" 	glyphicon glyphicon-modal-window"></span> Transparência</strong></button>
</div>
<div class="row">
    <button id='liga-heatmap' type="button" class="btn btn-warning active" onclick="toggleHeatmap()"> <span class="glyphicon glyphicon-globe"></span><strong id="heatmap-on-off"> Exibir Mapa de Calor</strong></button>
</div>