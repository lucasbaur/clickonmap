<div id='buscaNoGoogleMaps' class="row box search_box" style="float: right;">
    <form action="#" onsubmit="showAddress(this.address.value); return false">
        <fieldset class="form-group col-sm-9">
            <input type="text" id="geocode" class="form-control form-control-lg" name="address" placeholder="Informe um endereço" />
        </fieldset>

        <fieldset class="form-group col-sm-3" style="padding: 0px 5px;">
            <button id="buscar" type="submit" class="btn btn-warning active form-control"><strong> <span class="glyphicon glyphicon-search"></span> Buscar</strong></button>
        </fieldset>
    </form>
</div>